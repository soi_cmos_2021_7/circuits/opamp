EESchema Schematic File Version 4
LIBS:op2_tb-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L op2_tb-rescue:NMOS_MIN-MinedaSymbols M2
U 1 1 603B4F81
P 3200 2650
AR Path="/603B4F81" Ref="M2"  Part="1" 
AR Path="/603DAC44/603B4F81" Ref="M2"  Part="1" 
F 0 "M2" H 3444 2696 50  0000 L CNN
F 1 "NMOS_MIN" H 3550 2750 50  0001 L CNN
F 2 "" H 3200 2650 50  0001 C CNN
F 3 "" H 3200 2650 50  0001 C CNN
F 4 "M" H 3550 2450 50  0001 L CNN "Spice_Primitive"
F 5 "Nch l=10u w=20u" H 3444 2605 50  0000 L CNN "Spice_Model"
F 6 "Y" H 3550 2550 50  0001 L CNN "Spice_Netlist_Enabled"
	1    3200 2650
	1    0    0    -1  
$EndComp
$Comp
L op2_tb-rescue:NMOS_MIN-MinedaSymbols M3
U 1 1 603B62EC
P 3550 3350
AR Path="/603B62EC" Ref="M3"  Part="1" 
AR Path="/603DAC44/603B62EC" Ref="M3"  Part="1" 
F 0 "M3" H 3794 3396 50  0000 L CNN
F 1 "NMOS_MIN" H 3900 3450 50  0001 L CNN
F 2 "" H 3550 3350 50  0001 C CNN
F 3 "" H 3550 3350 50  0001 C CNN
F 4 "M" H 3900 3150 50  0001 L CNN "Spice_Primitive"
F 5 "Nch l=10u w=20u" H 3794 3305 50  0000 L CNN "Spice_Model"
F 6 "Y" H 3900 3250 50  0001 L CNN "Spice_Netlist_Enabled"
	1    3550 3350
	1    0    0    -1  
$EndComp
$Comp
L op2_tb-rescue:NMOS_MIN-MinedaSymbols M6
U 1 1 603B7A0D
P 4300 2650
AR Path="/603B7A0D" Ref="M6"  Part="1" 
AR Path="/603DAC44/603B7A0D" Ref="M6"  Part="1" 
F 0 "M6" H 4256 2696 50  0000 R CNN
F 1 "NMOS_MIN" H 4650 2750 50  0001 L CNN
F 2 "" H 4300 2650 50  0001 C CNN
F 3 "" H 4300 2650 50  0001 C CNN
F 4 "M" H 4650 2450 50  0001 L CNN "Spice_Primitive"
F 5 "Nch l=10u w=20u" H 4256 2605 50  0000 R CNN "Spice_Model"
F 6 "Y" H 4650 2550 50  0001 L CNN "Spice_Netlist_Enabled"
	1    4300 2650
	-1   0    0    -1  
$EndComp
$Comp
L MinedaSymbols:PMOS_MIN M5
U 1 1 603BAEEF
P 3900 2000
F 0 "M5" H 4144 2046 50  0000 L CNN
F 1 "PMOS_MIN" H 4250 2100 50  0001 L CNN
F 2 "" H 3900 2000 50  0001 C CNN
F 3 "" H 3900 2000 50  0001 C CNN
F 4 "M" H 4250 1800 50  0001 L CNN "Spice_Primitive"
F 5 "Pch l=10u w=40u" H 4144 1955 50  0000 L CNN "Spice_Model"
F 6 "Y" H 4250 1900 50  0001 L CNN "Spice_Netlist_Enabled"
	1    3900 2000
	1    0    0    -1  
$EndComp
$Comp
L MinedaSymbols:PMOS_MIN M4
U 1 1 603BD48E
P 3600 2000
F 0 "M4" H 3844 2046 50  0000 L CNN
F 1 "PMOS_MIN" H 3950 2100 50  0001 L CNN
F 2 "" H 3600 2000 50  0001 C CNN
F 3 "" H 3600 2000 50  0001 C CNN
F 4 "M" H 3950 1800 50  0001 L CNN "Spice_Primitive"
F 5 "Pch l=10u w=40u" H 3844 1955 50  0000 L CNN "Spice_Model"
F 6 "Y" H 3950 1900 50  0001 L CNN "Spice_Netlist_Enabled"
	1    3600 2000
	-1   0    0    -1  
$EndComp
$Comp
L op2_tb-rescue:NMOS_MIN-MinedaSymbols M1
U 1 1 603C734C
P 2950 3350
AR Path="/603C734C" Ref="M1"  Part="1" 
AR Path="/603DAC44/603C734C" Ref="M1"  Part="1" 
F 0 "M1" H 3194 3396 50  0000 L CNN
F 1 "NMOS_MIN" H 3300 3450 50  0001 L CNN
F 2 "" H 2950 3350 50  0001 C CNN
F 3 "" H 2950 3350 50  0001 C CNN
F 4 "M" H 3300 3150 50  0001 L CNN "Spice_Primitive"
F 5 "Nch l=10u w=20u" H 3194 3305 50  0000 L CNN "Spice_Model"
F 6 "Y" H 3300 3250 50  0001 L CNN "Spice_Netlist_Enabled"
	1    2950 3350
	-1   0    0    -1  
$EndComp
$Comp
L MinedaSymbols:RES_MIN R1
U 1 1 603CA54D
P 2750 1950
F 0 "R1" H 2828 1891 50  0000 L CNN
F 1 "RES_MIN" H 2828 1800 50  0000 L CNN
F 2 "" H 2800 1950 50  0001 C CNN
F 3 "" H 2800 1950 50  0001 C CNN
F 4 "R" H 2900 1600 50  0001 L CNN "Spice_Primitive"
F 5 "100k" H 2828 1709 50  0000 L CNN "Spice_Model"
F 6 "Y" H 2900 1700 50  0001 L CNN "Spice_Netlist_Enabled"
	1    2750 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 1550 2750 1950
Wire Wire Line
	2750 3150 3000 3150
Wire Wire Line
	3000 3350 2950 3350
Connection ~ 2750 3150
Wire Wire Line
	2750 3150 2750 3200
Wire Wire Line
	3000 3150 3000 3350
Wire Wire Line
	2750 2250 2750 3150
Wire Wire Line
	3000 3350 3550 3350
Connection ~ 3000 3350
Wire Wire Line
	2750 3500 2750 3600
Wire Wire Line
	2750 3600 3750 3600
Wire Wire Line
	3750 3600 3750 3500
Wire Wire Line
	3400 2800 3400 2950
Wire Wire Line
	3400 2950 3750 2950
Wire Wire Line
	4100 2950 4100 2800
Wire Wire Line
	3750 2950 3750 3200
Connection ~ 3750 2950
Wire Wire Line
	3750 2950 4100 2950
Wire Wire Line
	3400 2150 3400 2200
Wire Wire Line
	4100 2150 4100 2200
Wire Wire Line
	3600 2000 3650 2000
Wire Wire Line
	3400 2200 3650 2200
Wire Wire Line
	3650 2200 3650 2000
Connection ~ 3400 2200
Connection ~ 3650 2000
Wire Wire Line
	3650 2000 3900 2000
Wire Wire Line
	3400 2650 3500 2650
Wire Wire Line
	3500 2650 3500 2800
Wire Wire Line
	3500 2800 3400 2800
Connection ~ 3400 2800
Wire Wire Line
	4000 2650 4000 2800
Wire Wire Line
	4000 2800 4100 2800
Connection ~ 4100 2800
Wire Wire Line
	4000 2650 4100 2650
Wire Wire Line
	3750 3350 3800 3350
Wire Wire Line
	3800 3350 3800 3500
Wire Wire Line
	3800 3500 3750 3500
Connection ~ 3750 3500
Wire Wire Line
	2750 3350 2700 3350
Wire Wire Line
	2700 3350 2700 3500
Wire Wire Line
	2700 3500 2750 3500
Connection ~ 2750 3500
$Comp
L op2_tb-rescue:NMOS_MIN-MinedaSymbols M8
U 1 1 603D0078
P 4800 3350
AR Path="/603D0078" Ref="M8"  Part="1" 
AR Path="/603DAC44/603D0078" Ref="M8"  Part="1" 
F 0 "M8" H 5044 3396 50  0000 L CNN
F 1 "NMOS_MIN" H 5150 3450 50  0001 L CNN
F 2 "" H 4800 3350 50  0001 C CNN
F 3 "" H 4800 3350 50  0001 C CNN
F 4 "M" H 5150 3150 50  0001 L CNN "Spice_Primitive"
F 5 "Nch l=10u w=20u" H 5044 3305 50  0000 L CNN "Spice_Model"
F 6 "Y" H 5150 3250 50  0001 L CNN "Spice_Netlist_Enabled"
	1    4800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3350 4800 3350
Wire Wire Line
	5000 3600 5000 3500
Wire Wire Line
	5000 3350 5050 3350
Wire Wire Line
	5050 3350 5050 3500
Wire Wire Line
	5050 3500 5000 3500
Connection ~ 5000 3500
Wire Wire Line
	3000 3150 4250 3150
Wire Wire Line
	4250 3150 4250 3350
Connection ~ 3000 3150
$Comp
L MinedaSymbols:PMOS_MIN M7
U 1 1 603D20EF
P 4800 2200
F 0 "M7" H 5044 2246 50  0000 L CNN
F 1 "PMOS_MIN" H 5150 2300 50  0001 L CNN
F 2 "" H 4800 2200 50  0001 C CNN
F 3 "" H 4800 2200 50  0001 C CNN
F 4 "M" H 5150 2000 50  0001 L CNN "Spice_Primitive"
F 5 "Pch l=10u w=40u" H 5044 2155 50  0000 L CNN "Spice_Model"
F 6 "Y" H 5150 2100 50  0001 L CNN "Spice_Netlist_Enabled"
	1    4800 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2200 4800 2200
Connection ~ 4100 2200
Wire Wire Line
	4100 2200 4100 2400
Wire Wire Line
	5000 2350 5000 2400
Wire Wire Line
	3400 2000 3350 2000
Wire Wire Line
	3350 2000 3350 1850
Wire Wire Line
	3350 1850 3400 1850
Wire Wire Line
	4100 2000 4150 2000
Wire Wire Line
	4150 2000 4150 1850
Wire Wire Line
	4150 1850 4100 1850
Wire Wire Line
	5000 2200 5050 2200
Wire Wire Line
	3400 1550 3400 1850
Wire Wire Line
	4100 1550 4100 1850
Wire Wire Line
	5000 1550 5000 2050
Connection ~ 3750 3600
$Comp
L power:GND #PWR0102
U 1 1 603DCF99
P 3750 3850
F 0 "#PWR0102" H 3750 3600 50  0001 C CNN
F 1 "GND" H 3755 3677 50  0000 C CNN
F 2 "" H 3750 3850 50  0001 C CNN
F 3 "" H 3750 3850 50  0001 C CNN
	1    3750 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3600 3750 3850
Connection ~ 3400 1550
Wire Wire Line
	3400 1550 4100 1550
Connection ~ 3400 1850
Wire Wire Line
	2750 1550 3400 1550
Connection ~ 4100 1550
Connection ~ 4100 1850
Wire Wire Line
	4100 1550 5000 1550
Text Notes 4100 3900 0    50   ~ 0
.include models/SOI_CMOS
Wire Wire Line
	4300 2650 4450 2650
Wire Wire Line
	4450 2650 4450 2800
Wire Wire Line
	3200 2650 3100 2650
Connection ~ 5000 2650
Wire Wire Line
	5000 2650 5000 3200
$Comp
L MinedaSymbols:CAP_MIN C1
U 1 1 603B9E94
P 4600 2400
F 0 "C1" V 4906 2250 50  0000 C CNN
F 1 "CAP_MIN" V 4815 2250 50  0000 C CNN
F 2 "" H 4550 2470 50  0001 C CNN
F 3 "" H 4550 2470 50  0001 C CNN
F 4 "C" H 4700 2000 50  0001 L CNN "Spice_Primitive"
F 5 "10p" V 4724 2250 50  0000 C CNN "Spice_Model"
F 6 "Y" H 4700 2100 50  0001 L CNN "Spice_Netlist_Enabled"
	1    4600 2400
	0    -1   -1   0   
$EndComp
Connection ~ 5000 2400
Wire Wire Line
	5000 2400 5000 2650
Wire Wire Line
	5000 2050 5050 2050
Wire Wire Line
	5050 2050 5050 2200
Connection ~ 5000 2050
Wire Wire Line
	3400 2200 3400 2500
Wire Wire Line
	4100 2400 4300 2400
Connection ~ 4100 2400
Wire Wire Line
	4100 2400 4100 2500
Text Label 3200 2800 0    50   ~ 0
inm
Text Label 4450 2800 0    50   ~ 0
inp
$Comp
L MinedaSymbols:RES_MIN R4
U 1 1 603BF88A
P 4300 2400
F 0 "R4" H 4378 2341 50  0000 L CNN
F 1 "RES_MIN" H 4378 2250 50  0000 L CNN
F 2 "" H 4350 2400 50  0001 C CNN
F 3 "" H 4350 2400 50  0001 C CNN
F 4 "R" H 4450 2050 50  0001 L CNN "Spice_Primitive"
F 5 "10k" H 4378 2159 50  0000 L CNN "Spice_Model"
F 6 "Y" H 4450 2150 50  0001 L CNN "Spice_Netlist_Enabled"
	1    4300 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4900 2400 5000 2400
Wire Wire Line
	5000 2650 5650 2650
Text HLabel 5650 2650 2    50   Output ~ 0
out
Text HLabel 3100 2650 0    50   Input ~ 0
inm
Text HLabel 4450 2800 0    50   Input ~ 0
inp
Wire Wire Line
	3750 3600 5000 3600
Text HLabel 2600 1550 0    50   Input ~ 0
vdd
Wire Wire Line
	2600 1550 2750 1550
Connection ~ 2750 1550
$EndSCHEMATC
