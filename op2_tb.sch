EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 2600 3350
Text Notes 3200 3700 0    50   ~ 0
.include models/SOI_CMOS
$Comp
L Simulation_SPICE:VDC V1
U 1 1 603E06B5
P 2200 2850
F 0 "V1" H 2330 2941 50  0000 L CNN
F 1 "VDC" H 2330 2850 50  0000 L CNN
F 2 "" H 2200 2850 50  0001 C CNN
F 3 "~" H 2200 2850 50  0001 C CNN
F 4 "Y" H 2200 2850 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2200 2850 50  0001 L CNN "Spice_Primitive"
F 6 "dc(5)" H 2330 2759 50  0000 L CNN "Spice_Model"
	1    2200 2850
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:VDC V3
U 1 1 603E1957
P 3400 3000
F 0 "V3" H 3530 3091 50  0000 L CNN
F 1 "VDC" H 3530 3000 50  0000 L CNN
F 2 "" H 3400 3000 50  0001 C CNN
F 3 "~" H 3400 3000 50  0001 C CNN
F 4 "Y" H 3400 3000 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 3400 3000 50  0001 L CNN "Spice_Primitive"
F 6 "dc(2.5)" H 3530 2909 50  0000 L CNN "Spice_Model"
	1    3400 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 3200 3400 3350
Wire Wire Line
	2200 2150 2200 2650
Wire Wire Line
	2200 3350 2600 3350
Wire Wire Line
	2200 3050 2200 3350
$Comp
L Simulation_SPICE:VSIN V2
U 1 1 603E5D67
P 2600 2600
F 0 "V2" H 2730 2691 50  0000 L CNN
F 1 "VSIN" H 2730 2600 50  0000 L CNN
F 2 "" H 2600 2600 50  0001 C CNN
F 3 "~" H 2600 2600 50  0001 C CNN
F 4 "Y" H 2600 2600 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2600 2600 50  0001 L CNN "Spice_Primitive"
F 6 "AC 1 sin(2.5 0.1 1k)" H 2730 2509 50  0000 L CNN "Spice_Model"
	1    2600 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2300 3100 2300
Wire Wire Line
	2600 2300 2600 2400
Wire Wire Line
	2600 2800 2600 3350
$Comp
L op2_tb-rescue:RES_MIN-MinedaSymbols R2
U 1 1 603E822B
P 2800 2300
F 0 "R2" V 3105 2150 50  0000 C CNN
F 1 "RES_MIN" V 3014 2150 50  0000 C CNN
F 2 "" H 2850 2300 50  0001 C CNN
F 3 "" H 2850 2300 50  0001 C CNN
F 4 "R" H 2950 1950 50  0001 L CNN "Spice_Primitive"
F 5 "10k" V 2923 2150 50  0000 C CNN "Spice_Model"
F 6 "Y" H 2950 2050 50  0001 L CNN "Spice_Netlist_Enabled"
	1    2800 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2800 2300 2600 2300
Wire Wire Line
	3200 2300 3200 3450
Text GLabel 5050 2350 2    50   Output ~ 0
out
Wire Wire Line
	4650 2350 5050 2350
Text Label 3400 2850 0    50   ~ 0
inp
Wire Wire Line
	2600 3350 3400 3350
$Sheet
S 3600 2100 800  550 
U 603DAC44
F0 "Sheet603DAC43" 50
F1 "op2.sch" 50
F2 "out" I R 4400 2350 50 
F3 "inm" I L 3600 2300 50 
F4 "inp" I L 3600 2500 50 
F5 "vdd" I L 3600 2150 50 
$EndSheet
Wire Wire Line
	2200 2150 3600 2150
Wire Wire Line
	3200 2300 3600 2300
Connection ~ 3200 2300
Wire Wire Line
	3400 2500 3600 2500
Wire Wire Line
	3400 2500 3400 2800
Wire Wire Line
	4400 2350 4650 2350
Connection ~ 4650 2350
Wire Wire Line
	3200 3450 3800 3450
Wire Wire Line
	4650 3450 4650 2350
$Comp
L op2_tb-rescue:RES_MIN-MinedaSymbols R3
U 1 1 603EA32B
P 3800 3450
F 0 "R3" H 3878 3391 50  0000 L CNN
F 1 "RES_MIN" H 3878 3300 50  0000 L CNN
F 2 "" H 3850 3450 50  0001 C CNN
F 3 "" H 3850 3450 50  0001 C CNN
F 4 "R" H 3950 3100 50  0001 L CNN "Spice_Primitive"
F 5 "100k" H 3878 3209 50  0000 L CNN "Spice_Model"
F 6 "Y" H 3950 3200 50  0001 L CNN "Spice_Netlist_Enabled"
	1    3800 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 3450 4650 3450
Wire Wire Line
	2600 3350 2600 3600
$Comp
L power:GND #PWR0101
U 1 1 603E7228
P 2600 3600
F 0 "#PWR0101" H 2600 3350 50  0001 C CNN
F 1 "GND" H 2605 3427 50  0000 C CNN
F 2 "" H 2600 3600 50  0001 C CNN
F 3 "" H 2600 3600 50  0001 C CNN
	1    2600 3600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
