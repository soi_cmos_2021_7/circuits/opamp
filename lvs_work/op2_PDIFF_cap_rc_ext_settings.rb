def rc_ext_settings
  same_circuits 'op2_aps_PADS', 'OP2_APSCAP_PADS'
  #same_circuits  'op2_core', 'OP2_APSCAP'
  netlist.flatten_circuit 'ESD_PAD'
  align
  same_device_classes 'PMOS', 'PCH'
  same_device_classes 'NMOS', 'NCH'
  same_device_classes 'NRES', 'RES'
  same_device_classes 'PRES', 'RES'
  same_device_classes 'R-SOI', 'RES'
  tolerance 'NRES', 'R', relative: 0.03
  tolerance 'PRES', 'R', relative: 0.2
  tolerance 'R-SOI', 'R', relative: 0.03
  same_device_classes 'MIMCAP', 'CAP'
  same_device_classes 'PDIFFCAP', 'CAP'
  same_device_classes 'NDIFFCAP', 'CAP'
  same_device_classes 'TiNCAP', 'CAP'
  tolerance 'MIMCAP', 'C', relative: 0.03
  tolerance 'PDIFFCAP', 'C', relative: 0.03
  tolerance 'NDIFFCAP', 'C', relative: 0.03
  tolerance 'TiNCAP', 'C', relative: 0.03
  same_device_classes 'D', 'PDIO'
  same_device_classes 'D', 'NDIO'
  same_device_classes 'D', 'D'
  netlist.make_top_level_pins
  netlist.combine_devices
  schematic.combine_devices
end
