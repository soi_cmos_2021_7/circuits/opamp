def rc_ext_settings
  same_circuits 'op2', '.TOP'
  align
  same_device_classes 'NMOS', 'NCH'
  same_device_classes 'PMOS', 'PCH'
  same_device_classes 'NRES', 'RES'
  same_device_classes 'PRES', 'RES'
  tolerance 'NRES', 'R', relative: 0.03
  tolerance 'PRES', 'R', relative: 0.1
  same_device_classes 'MIMCAP', 'CAP'
  same_device_classes 'PDIFFCAP', 'CAP'
  tolerance 'MIMCAP', 'C', relative: 0.03
  tolerance 'PDIFFCAP', 'C', relative: 0.1
  # netlist.flatten_circuit 'Nch*'
  # netlist.flatten_circuit 'PMOS*'
  # netlist.flatten_circuit 'R_poly*'
  # netlist.flatten_circuit 'HR_poly*'
  netlist.combine_devices
  schematic.combine_devices
end
