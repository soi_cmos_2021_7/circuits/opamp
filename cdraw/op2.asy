Version 4
SymbolType BLOCK
LINE Normal 66 -16 -64 -63
LINE Normal -63 32 66 -16
RECTANGLE Normal -64 -64 65 32
WINDOW 0 0 -56 Bottom 2
PIN -64 -32 LEFT 8
PINATTR PinName inm
PINATTR SpiceOrder 1
PIN -64 0 LEFT 8
PINATTR PinName inp
PINATTR SpiceOrder 2
PIN 0 -64 LEFT 8
PINATTR PinName Vdd
PINATTR SpiceOrder 3
PIN 64 -16 RIGHT 8
PINATTR PinName out
PINATTR SpiceOrder 4
PIN 0 32 LEFT 8
PINATTR PinName vbias
PINATTR SpiceOrder 5
