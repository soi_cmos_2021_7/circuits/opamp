Version 4
SymbolType CELL
LINE Normal -16 -3 16 -3
LINE Normal -10 4 -17 4
LINE Normal 3 4 -4 4
LINE Normal 16 4 9 4
CIRCLE Normal -32 -32 32 32
WINDOW 0 32 -32 Left 2
WINDOW 3 32 0 Left 2
PIN 0 -64 NONE 0
PINATTR PinName 
PINATTR SpiceOrder 1
PIN 0 64 NONE 0
PINATTR PinName 
PINATTR SpiceOrder 2
